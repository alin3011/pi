

#include "stdafx.h"
#include "common.h"
#include "string.h"

/// Global Variables
const int alpha_slider_max = 255;
int alpha_slider;
double alpha;
double beta;
Mat srcTrack;

void testOpenImage()
{
	char fname[MAX_PATH];
	while(openFileDlg(fname))
	{
		Mat src;
		src = imread(fname);
		imshow("image",src);
		waitKey();
	}
}


void splitRGB()
{
	Mat src;
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		src = imread(fname);
		Mat red(src.rows, src.cols, CV_8UC1);
		Mat green(src.rows, src.cols, CV_8UC1);
		Mat blue(src.rows, src.cols, CV_8UC1);
		for (int i = 0; i < src.rows; i++){
			for (int j = 0; j < src.cols; j++){
				Vec3b pixel = src.at< Vec3b>(i, j);
				blue.at<uchar>(i, j) = pixel[0];
				red.at<uchar>(i, j) = pixel[2];
				green.at<uchar>(i, j) = pixel[1];
			}
		}
		imshow("image", src);
		imshow("blue", blue);
		imshow("green", green);
		imshow("red", red);
		waitKey();
	}
}

void splitRGBv2()
{
	Mat src;
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		src = imread(fname);
		Mat red(src.rows, src.cols, CV_8UC3);
		Mat green(src.rows, src.cols, CV_8UC3);
		Mat blue(src.rows, src.cols, CV_8UC3);
		for (int i = 0; i < src.rows; i++){
			for (int j = 0; j < src.cols; j++){
				Vec3b pixel = src.at< Vec3b>(i, j);
				blue.at<Vec3b>(i, j) = Vec3b(pixel[0],0,0);
				red.at<Vec3b>(i, j) = Vec3b(0, 0, pixel[2]);
				green.at<Vec3b>(i, j) = Vec3b(0, pixel[1], 0);
			}
		}
		imshow("image", src);
		imshow("blue", blue);
		imshow("green", green);
		imshow("red", red);
		waitKey();
	}
}


void colorGray()
{
	Mat src;
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		src = imread(fname);
		Mat grayImg(src.rows, src.cols, CV_8UC1);
		for (int i = 0; i < src.rows; i++){
			for (int j = 0; j < src.cols; j++){
				Vec3b pixel = src.at< Vec3b>(i, j);
				grayImg.at<uchar>(i, j) = (pixel[0] + pixel[1] + pixel[2]) / 3;
			}
		}
		imshow("image", src);
		imshow("gray", grayImg);
		waitKey();
	}
}

void colorBlackWhite(int threshold)
{
	Mat src;
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		src = imread(fname);
		Mat grayImg(src.rows, src.cols, CV_8UC1);
		for (int i = 0; i < src.rows; i++){
			for (int j = 0; j < src.cols; j++){
				Vec3b pixel = src.at< Vec3b>(i, j);
				if ((pixel[0] + pixel[1] + pixel[2]) / 3 > threshold) grayImg.at<uchar>(i, j) = 255;
				else grayImg.at<uchar>(i, j) = 0;
			}
		}
		imshow("image", src);
		imshow("gray", grayImg);
		waitKey();
	}
}

void colorBlackWhiteTrack(int threshold)
{

		Mat grayImg(srcTrack.rows, srcTrack.cols, CV_8UC3);
		for (int i = 0; i < srcTrack.rows; i++){
			for (int j = 0; j < srcTrack.cols; j++){
				Vec3b pixel = srcTrack.at< Vec3b>(i, j);
				if ((pixel[0] + pixel[1] + pixel[2]) / 3 > threshold) grayImg.at<Vec3b>(i, j) = Vec3b(255,255,255);
				else grayImg.at<Vec3b>(i, j) = Vec3b(0, 0, 0);
			}
		}
		putText(grayImg, std::to_string(alpha_slider), Point(20, 20), FONT_HERSHEY_COMPLEX, 1, Scalar(0,0,255));
		imshow("image", srcTrack);
		imshow("gray", grayImg);
		waitKey(0);
	
} 

void colorHSV()
{
	Mat src;
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		src = imread(fname);
		Mat h(src.rows, src.cols, CV_8UC1);
		Mat s(src.rows, src.cols, CV_8UC1);
		Mat v(src.rows, src.cols, CV_8UC1);
		for (int i = 0; i < src.rows; i++){
			for (int j = 0; j < src.cols; j++){
				Vec3b pixel = src.at< Vec3b>(i, j);
				float r = (float)pixel[2] / 255; // r : the normalized R component
				float g = (float)pixel[1] / 255; // g : the normalized G component
				float b = (float)pixel[0] / 255; // b : the normalized B component 
				float M = max(r, max(g ,b));
				float m = min(r, min(g, b));
				float C = M - m;
				float V = M;
				float S;
				if(C) S = C / V;
				else S = 0;
				float H;
				if(C) {
					if (M == r) H = 60 * (g - b) / C;
					if (M == g) H = 120 + 60 * (b - r) / C;
					if (M == b) H = 240 + 60 * (r - g) / C;
				}
				else // grayscale
					H = 0;
				if(H < 0)
					H = H + 360;
				h.at<uchar>(i, j) = (H * 255 / 360);
				s.at<uchar>(i, j) = (S * 255);
				v.at<uchar>(i, j) = (V * 255);
			}
		}
		imshow("image", src);
		imshow("h", h);
		imshow("s", s);
		imshow("v", v);
		waitKey();
	}
}

void rotate()
{
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src , temp;
		src = imread(fname);
		temp = src.clone();
		imshow("image", temp);

		transpose(temp, src);
		imshow("image2", src);
		waitKey();
	}
}

void spin()
{
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src, temp;
		src = imread(fname);
		temp = src.clone();
		imshow("image", temp);
		while (waitKey(1000) != 48){
			transpose(temp, src);
			flip(temp, src, 1);
			imshow("image2", src);
			temp = src.clone();
			waitKey();
		}
	}
}

void text()
{
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src;
		src = imread(fname);
		putText(src, "abc", Point(20, 20), FONT_HERSHEY_COMPLEX, 1, Scalar());
		imshow("image", src);
		waitKey();
	}
}

void testOpenImagesFld()
{
	char folderName[MAX_PATH];
	if (openFolderDlg(folderName)==0)
		return;
	char fname[MAX_PATH];
	FileGetter fg(folderName,"bmp");
	while(fg.getNextAbsFile(fname))
	{
		Mat src;
		src = imread(fname);
		imshow(fg.getFoundFileName(),src);
		if (waitKey()==27) //ESC pressed
			break;
	}
}


void testResize()
{
	char fname[MAX_PATH];
	while(openFileDlg(fname))
	{
		Mat src;
		src = imread(fname);
		Mat dst1,dst2;
		//without interpolation
		resizeImg(src,dst1,320,false);
		//with interpolation
		resizeImg(src,dst2,320,true);
		imshow("input image",src);
		imshow("resized image (without interpolation)",dst1);
		imshow("resized image (with interpolation)",dst2);
		waitKey();
	}
}


void testVideoSequence()
{
	VideoCapture cap("Videos/rubic.avi"); // off-line video from file
	//VideoCapture cap(0);	// live video from web cam
	if (!cap.isOpened()) {
		printf("Cannot open video capture device.\n");
		waitKey(0);
		return;
	}
		
	Mat edges;
	Mat frame;
	char c;

	while (cap.read(frame))
	{
		Mat grayFrame;
		cvtColor(frame, grayFrame, CV_BGR2GRAY);
		imshow("source", frame);
		imshow("gray", grayFrame);
		c = cvWaitKey(0);  // waits a key press to advance to the next frame
		if (c == 27) {
			// press ESC to exit
			printf("ESC pressed - capture finished\n"); 
			break;  //ESC pressed
		};
	}
}


void testSnap()
{
	VideoCapture cap(0); // open the deafult camera (i.e. the built in web cam)
	if (!cap.isOpened()) // openenig the video device failed
	{
		printf("Cannot open video capture device.\n");
		return;
	}

	Mat frame;
	char numberStr[256];
	char fileName[256];
	
	// video resolution
	Size capS = Size((int)cap.get(CV_CAP_PROP_FRAME_WIDTH),
		(int)cap.get(CV_CAP_PROP_FRAME_HEIGHT));

	// Display window
	const char* WIN_SRC = "Src"; //window for the source frame
	namedWindow(WIN_SRC, CV_WINDOW_AUTOSIZE);
	cvMoveWindow(WIN_SRC, 0, 0);

	const char* WIN_DST = "Snapped"; //window for showing the snapped frame
	namedWindow(WIN_DST, CV_WINDOW_AUTOSIZE);
	cvMoveWindow(WIN_DST, capS.width + 10, 0);

	char c;
	int frameNum = -1;
	int frameCount = 0;

	for (;;)
	{
		cap >> frame; // get a new frame from camera
		if (frame.empty())
		{
			printf("End of the video file\n");
			break;
		}

		++frameNum;
		
		imshow(WIN_SRC, frame);

		c = cvWaitKey(10);  // waits a key press to advance to the next frame
		if (c == 27) {
			// press ESC to exit
			printf("ESC pressed - capture finished");
			break;  //ESC pressed
		}
		if (c == 115){ //'s' pressed - snapp the image to a file
			frameCount++;
			fileName[0] = NULL;
			sprintf(numberStr, "%d", frameCount);
			strcat(fileName, "Images/A");
			strcat(fileName, numberStr);
			strcat(fileName, ".bmp");
			bool bSuccess = imwrite(fileName, frame);
			if (!bSuccess) 
			{
				printf("Error writing the snapped image\n");
			}
			else
				imshow(WIN_DST, frame);
		}
	}

}

void MyCallBackFunc(int event, int x, int y, int flags, void* param)
{
	//More examples: http://opencvexamples.blogspot.com/2014/01/detect-mouse-clicks-and-moves-on-image.html
	Mat* src = (Mat*)param;
	if (event == CV_EVENT_LBUTTONDOWN)
		{
			printf("Pos(x,y): %d,%d  Color(RGB): %d,%d,%d\n",
				x, y,
				(int)(*src).at<Vec3b>(y, x)[2],
				(int)(*src).at<Vec3b>(y, x)[1],
				(int)(*src).at<Vec3b>(y, x)[0]);
		}
}

void testMouseClick()
{
	Mat src;
	// Read image from file 
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		src = imread(fname);
		//Create a window
		namedWindow("My Window", 1);

		//set the callback function for any mouse event
		setMouseCallback("My Window", MyCallBackFunc, &src);

		//show the image
		imshow("My Window", src);

		// Wait until user press some key
		waitKey(0);
	}
}

void negativeImage(){
	Mat img;
	Mat src;
	char fname[MAX_PATH];
	
	while (openFileDlg(fname))
	{
		img = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		src = img.clone();
		imshow("original", src);
		for (int i = 0; i < img.rows; i++){
			for (int j = 0; j < img.cols; j++){
				img.at<uchar>(i, j) = 255 - img.at<uchar>(i, j);
			}
		}
		imshow("My Window", img);
	}
	waitKey(0);
}

void aditiveImage(int adition){
	Mat img;
	Mat src;
	char fname[MAX_PATH];

	while (openFileDlg(fname))
	{
		img = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		src = img.clone();
		imshow("original", src);
		for (int i = 0; i < img.rows; i++){
			for (int j = 0; j < img.cols; j++){
				if (img.at<uchar>(i, j) + adition >= 255) img.at<uchar>(i, j) = 255;
				else img.at<uchar>(i, j) = img.at<uchar>(i, j) + adition;
			}
		}
		imshow("My Window", img);
	}
	waitKey(0);
}

void multiplicativeImage(float multi){
	Mat img;
	Mat src;
	char fname[MAX_PATH];

	while (openFileDlg(fname))
	{
		img = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		src = img.clone();
		imshow("original", src);
		for (int i = 0; i < img.rows; i++){
			for (int j = 0; j < img.cols; j++){
				if (img.at<uchar>(i, j) * multi >= 255) img.at<uchar>(i, j) = 255;
				else img.at<uchar>(i, j) = img.at<uchar>(i, j) * multi;
			}
		}
		imshow("My Window", img);
		imwrite("newPic", img);
	}
	waitKey(0);
}


void createImage(){
	Mat img(256, 256, CV_8UC3);
	Vec3b white(255, 255, 255);
	Vec3b red(0, 0, 255);
	Vec3b yellow(0, 255, 255);
	Vec3b green(0, 255, 0);
	for (int i = 0; i < img.rows; i++){
			for (int j = 0; j < img.cols; j++){
				Vec3b pixel = img.at< Vec3b>(i, j);
				if (i < 128){
					if (j < 128) {
						img.at< Vec3b>(i, j) = white;
					}
					if (j > 128) {
						img.at< Vec3b>(i, j) = red;
					}
				}
				else{
					if (j < 128) {
						img.at< Vec3b>(i, j) = yellow;
					}
					if (j > 128) {
						img.at< Vec3b>(i, j) = green;
					}
				}
			}
		}
	imshow("My Window", img);

	waitKey(0);
}

void animate(){
	Mat img(256, 256, CV_8UC3);
	Vec3b white(255, 255, 255);
	Vec3b red(0, 0, 255);
	Vec3b yellow(0, 255, 255);
	Vec3b green(0, 255, 0);
	Vec3b colors[4] = { white, red, yellow, green };
	int index = 0;
	char end = 50;
	while (end  != 48){
		end = waitKey(1000);
		for (int i = 0; i < img.rows; i++){
			for (int j = 0; j < img.cols; j++){
				if (i < 128){
					if (j < 128) {
						img.at< Vec3b>(i, j) = colors[index % 4];
					}
					if (j > 128) {
						img.at< Vec3b>(i, j) = colors[(index + 1) % 4];
					}
				}
				else{
					if (j < 128) {
						img.at< Vec3b>(i, j) = colors[(index + 3) % 4];
					}
					if (j > 128) {
						img.at< Vec3b>(i, j) = colors[(index + 2) % 4];
					}
				}
				
			}
			
		}
		index++;
		imshow("My Window", img);
	}
	

	waitKey(0);
}




void disp(){
	Mat m(3, 3, CV_32FC1);
	Mat mtemp(3, 3, CV_32FC1);
	for (int i = 0; i < m.rows; i++){
		for (int j = 0; j < m.cols; j++){
			m.at<float>(i, j) = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 100));
		}
	}
	char c;
	std::cout << m;
    mtemp = m.inv();	
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;


	std::cout << mtemp;
	mtemp = m * mtemp;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << mtemp;

	std::cin >> c;
	waitKey(0);
}

/* Histogram display function - display a histogram using bars (simlilar to L3 / PI)
Input:
name - destination (output) window name
hist - pointer to the vector containing the histogram values
hist_cols - no. of bins (elements) in the histogram = histogram image width
hist_height - height of the histogram image
Call example:
showHistogram ("MyHist", hist_dir, 255, 200);
*/
void showHistogram(const string& name, int* hist, const int  hist_cols, const int hist_height)
{
	Mat imgHist(hist_height, hist_cols, CV_8UC3, CV_RGB(255, 255, 255)); // constructs a white image

	//computes histogram maximum
	int max_hist = 0;
	for (int i = 0; i<hist_cols; i++)
	if (hist[i] > max_hist)
		max_hist = hist[i];
	double scale = 1.0;
	scale = (double)hist_height / max_hist;
	int baseline = hist_height - 1;

	for (int x = 0; x < hist_cols; x++) {
		Point p1 = Point(x, baseline);
		Point p2 = Point(x, baseline - cvRound(hist[x] * scale));
		line(imgHist, p1, p2, CV_RGB(255, 0, 255)); // histogram bins colored in magenta
	}

	imshow(name, imgHist);
}

int *histogram(Mat img){
	
	int *rez;
	rez = (int *)calloc(256, sizeof(int));
		for (int i = 0; i < img.rows; i++)
			for (int j = 0; j < img.cols; j++){
				rez[img.at<uchar>(i, j)]++;
			}
	
	return rez;
}

void histRedus(int m){
	Mat img;
	char fname[MAX_PATH];
	int *rez;
	rez = (int *)calloc(256, sizeof(int));
	openFileDlg(fname);

	img = imread(fname);
	for (int i = 0; i < img.rows; i++)
		for (int j = 0; j < img.cols; j++){
			rez[img.at<uchar>(i, j) % m]++;
		}

	showHistogram("histogram", rez, 256 , 256 );
	waitKey(0);
}

float *fdp(Mat img)
{

	int *rez;
	float *rezFinal;
	rez = (int *)calloc(256, sizeof(int));
	rezFinal = (float *)calloc(256, sizeof(float));

	for (int i = 0; i < img.rows; i++)
		for (int j = 0; j < img.cols; j++){
			rez[img.at<uchar>(i, j)]++;
			
		}
	int m;

	m = (img.size().height * img.size().width);
	printf("--%d--    ", m);
	for (int i = 0; i < 256; i++){
		rezFinal[i] =rez[i] / m;
		printf("--%f--", rezFinal[i]);
	}
	return rezFinal;

}



void prag(){
	float *f;
	int latime = 11;
	int wh = 5;
	float th = 0.0003;
	float suma = 0;
	float media = 0;
	float max = 0;
	int n = 1;
	int *rez;
	rez = (int*)calloc(255, sizeof(int));
	char fname[MAX_PATH];
	openFileDlg(fname);
	Mat img;
	img = imread(fname);
	f = fdp(img);
	for (int i = wh; i <= 255 - wh; i++){
		for (int j = i - wh; j <= i + wh; j++){
			suma += f[j];
			//printf("%f", suma);
			if (f[j] > max) max = f[j];
		}
		media = suma / (2 * wh + 1);
		//printf("%d" , media);
		if (f[i] > (media + th) && f[i] > max) {
			rez[n] = i;
			n++;
		}
		max = 0;
		suma = 0;
	}
	rez[0] = 0;
	rez[n] = 255;
	for (int i = 0; i <= n; i++)printf("%d" , rez[i]);
	int maximClose;
	int *h;
	h = histogram(img);
	for (int i = 0; i < img.rows; i++)
		for (int j = 0; i < img.cols; j++){
			for (int k = 0; k <= n; k++){
				if (img.at<uchar>(i, j) > rez[k]) maximClose = rez[k - 1];
				else maximClose = 0;
			}
			img.at<uchar>(i, j) = h[maximClose];
		}
	imshow("pic", img);
}

void on_trackbar(int, void*)
{
	colorBlackWhiteTrack(alpha_slider);
}

int main()
{
	int op , add ;
	float mul;
	do
	{
		//system("cls");
		destroyAllWindows();
		printf("Menu:\n");
		printf(" 1 - Open image\n");
		printf(" 2 - Open BMP images from folder\n");
		printf(" 3 - Resize image\n");
		printf(" 4 - Process video\n");
		printf(" 5 - Snap frame from live video\n");
		printf(" 6 - Mouse callback demo\n");
		printf(" 7 - Negative image\n");
		printf(" 8 - Aditive image\n");
		printf(" 9 - Multiplicative image\n");
		printf(" 10 - create image\n");
		printf(" 11 - Matrix\n");
		printf(" 12 - Text on image\n");
		printf(" 0 - Exit\n\n");
		printf("Option: ");
		scanf("%d",&op);
		switch (op)
		{
			case 1:
				splitRGBv2();
				break;
			case 2:
				testOpenImagesFld();
				break;
			case 3:
				testResize();
				break;
			case 4:
				testVideoSequence();
				break;
			case 5:
				testSnap();
				break;
			case 6:
				testMouseClick();
				break;
			case 7:
				negativeImage();
				break;
			case 8:
				printf(" \n\n aditive factor : ");
				scanf("%d", &add);
				aditiveImage(add);
				break;
			case 9:
				printf(" \n\n multiplicative factor : ");
				scanf("%f", &mul);
				multiplicativeImage(mul);
				break;
			case 10:
				createImage();
				break;
			case 11:
				disp();
				break;
			case 12:
				text();
				break;
			case 13:
				rotate();
				break;
			case 14:
				spin();
				break;
			case 15:

				char fname[MAX_PATH];
				openFileDlg(fname);
				srcTrack = imread(fname);
				imshow("gray", srcTrack);
				namedWindow("gray", 1);
				char TrackbarName[50];
				sprintf(TrackbarName, "%d", alpha_slider);
				createTrackbar(TrackbarName, "gray", &alpha_slider, alpha_slider_max, on_trackbar);
				
				waitKey(0);
				break;
			case 16 : 
				histRedus(100);
				break;
			case 17:
				Mat img;
				//char fname[MAX_PATH];
				openFileDlg(fname);
				img = imread(fname);
				fdp(img);
				waitKey(0);
				break;

		}
	}
	while (op!=0);
	return 0;
}